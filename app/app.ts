import express from 'express';

const app: express.Application = express();

app.get('/', (req, res) => {
    res.send('Hello From the other side !!');
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`app is up and running on port ${PORT}`);
});
